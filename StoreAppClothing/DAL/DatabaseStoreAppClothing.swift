//
//  DatabaseStoreAppClothing.swift
//  StoreAppClothing
//
//  Created by Don on 5/25/22.
//

import Foundation
import UIKit
import os.log

class DatabaseStoreAppClothing {
    //MARK: Database Properties
    let dPath: String
    let DB_NAME: String = "Clothing.sqlite"
    let db: FMDatabase?
    
    //MARK: Tables's properties
    let TABLE_NAME: String = "product"
    let TABLE_ID: String = "_id"
    let PRODUCT_NAME: String = "name"
    let PRODUCT_IMAGE: String = "image"
    let PRODUCT_RATING: String = "rating"
    
    //MARK: Database Primitives
    init() {
        let directories:[String] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, true)
        dPath = directories[0] + "/" + DB_NAME
        db = FMDatabase(path: dPath)
        if db == nil {
            os_log("Can not create the database. Please review the dPath!")
        }
        else {
            os_log("Database is created successful!")
        }
    }
    // Create table
    func createTable() -> Bool {
        var ok: Bool = false
        
        if db != nil {
            let sql = "CREATE TABLE " + TABLE_NAME + "( "
                + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PRODUCT_NAME + " TEXT, "
                + PRODUCT_IMAGE + " TEXT, "
                + PRODUCT_RATING + " INTEGER)"
            
            if db!.executeStatements(sql) {
                ok = true
                os_log("Table is created!")
            }
            else {
                os_log("Can not create the table!")
            }
        }
        else {
            os_log("Database is nil!")
        }
        
        return ok
    }
    // Open database
    func open() -> Bool {
        var ok: Bool = false
        
        if db != nil {
            if db!.open() {
                ok = true
                os_log("The database is opened!")
            }
            else {
                print("Can not open the Database: \(db!.lastErrorMessage())")
            }
        }
        else {
            os_log("Database is nil!")
        }
        
        return ok
    }
    // Close database
    func close(){
        if db != nil {
            db!.close()
        }
    }
    
    //MARK: Database APIs
    func insertProduct(product: Product){
        if db != nil{
            // Transform the meal image to String
            let imageData: NSData = (product.image?.pngData()!)! as NSData
            let productImageString = imageData.base64EncodedData(options: .lineLength64Characters)
            let sql = "INSERT INTO " + TABLE_NAME + "(" + PRODUCT_NAME + ", " + PRODUCT_IMAGE + ", " + PRODUCT_RATING + ")" + " VALUES (?, ?, ?)"
            if db!.executeUpdate(sql, withArgumentsIn: [product.productName, productImageString, product.rating]) {
                os_log("The meal is insert to the database!")
            }
            else {
                os_log("Fail to insert the meal!")
            }
        }
        else {
            os_log("Database is nil!")
        }
    }
    
    func deleteProduct(product: Product){
        if db != nil {
            let sql = "DELETE FROM \(TABLE_NAME) WHERE \(PRODUCT_NAME) = ? AND \(PRODUCT_RATING) = ?"
            do {
                try db!.executeUpdate(sql, values: [product.productName, product.rating])
                os_log("The meal is deleted!")
            }
            catch {
                os_log("Fail to delete the meal!")
            }
        }
        else {
            os_log("Database is nil!")
        }
    }
    func readProductList(products:inout [Product]){
        if db != nil {
            var results: FMResultSet?
            let sql = "SELECT * FROM \(TABLE_NAME)"
            
            // Query
            do {
                results = try db!.executeQuery(sql, values: nil)
            }
            catch {
                print("Fail to read data: \(error.localizedDescription)")
            }
            // Read data from the results
            if results != nil {
                while (results?.next())! {
                    let productName = results!.string(forColumn: PRODUCT_NAME)
                    let stringImage = results!.string(forColumn: PRODUCT_IMAGE)
                    let productRating = results!.int(forColumn: PRODUCT_RATING)
                    // Transform string image to UIImage
                    let dataImage: Data = Data(base64Encoded: stringImage!, options: .ignoreUnknownCharacters)!
                    let productImage = UIImage(data: dataImage)
                    // Create a product to contain the values
                    let product = Product()
                        //Product(images: productImage, productName: productName, rating: Int(productRating), productPrices: 0, productDescription: "")
                       
                    products.append(product)
                }
            }
        }
        else{
            os_log("Database is nil!")
        }
    }
    
    func updateProduct(oldProduct: Product, newProduct: Product){
        if db != nil {
            let sql = "UPDATE \(TABLE_NAME) SET \(PRODUCT_NAME) = ?, \(PRODUCT_IMAGE) = ?, \(PRODUCT_RATING) = ? WHERE \(PRODUCT_NAME) = ? AND \(PRODUCT_RATING) = ?"
            // Transform image of new meal to String
            let newImageData: NSData = newProduct.image!.pngData()! as NSData
            let newStringImage = newImageData.base64EncodedString(options: .lineLength64Characters)
            // Try to query the database
            do{
                try db!.executeUpdate(sql, values: [newProduct.productName!, newStringImage, newProduct.rating!, oldProduct.productName!, oldProduct.rating!])
                os_log("Successful to update the meal!")
            }
            catch{
                print("Fail to update the meal: \(error.localizedDescription)")
            }
        }
        else {
            os_log("Database is nil!")
        }
    }
}
